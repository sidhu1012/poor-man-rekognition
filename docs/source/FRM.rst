Face Recognition Module (FRM)
=============================

------------

This class runs through the entire procedure of obtaining the videos
as scenes, and then running them through the multiple submodules, to finally
obtain the `result`: the amount of detected bounding boxes, and the amound of
recognized bounding boxes.

Although ``FRM`` can be used individually, it is mainly, as mentioned in the
proposal, a major submodule of the Video Processing Module (VPM). FRM concerns itself
with the tasks of doing all the work with respect to faces. Every aspect of FRM
is related to some processing of the face, unlike VPM which deals with other aspects
of the Rekognition pipeline.

FRM consists of one class named ``FRM``.

.. autoclass:: rekognition.FRM

-------------

Shot Segmentation
-----------------

Shot segmentation is the task of partitioning a video clip, such that
each partition is a whole scene (starts with a cut, ends with a cut). Shot segmentation
never directly interacts with FRM; but in the Rekognition pipeline, both share a common high-layer
of the VPM.

The Shot Segmentation base class, ``ShotSegmentation``, is a very simple one. In Rekognition, there is currently one segmentation method: ``PSDSegmenter``.

``PSDSegmenter``
````````````````

.. note:: ``PSDSegmenter`` is a wrapper around PySceneDetect.

.. autoclass:: rekognition.shot_segmentation.PSDSegmenter
