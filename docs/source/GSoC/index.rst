GSoC
====

This page contains the various docs pertaining to the GSoC program. It includes pages describing the progress across a timeline and also explanations regarding the personal preferences involved in the architecture of the module. If you haven't already, go through my initial proposal (``docs/Proposal.pdf``) which contains an extensive description of the complete system as well as the deadlines set up for various tasks.

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   eval_one
   eval_two
   eval_three
