Video Processing Module (VPM)
=============================

.. automodule:: rekognition.VPM
   :undoc-members:

------------

The Video Processing Module (VPM) is the higher most level of the Rekognition
architecture, and integrates the facial processing (FRM) with Video Generation and Shot
Segmentation.

The special feature of VPM is in its Bounding Box Coding, which helps it to easily
receive feedback without the need of tight coupling with a UI. Each bounding box is
represented by a triplet of ``s:f:b``, where ``s`` is the scene the box is in, ``f`` is the
:math:`f^\text{th}` frame of ``ss``, and ``b`` is the :math:`b^\text{th}` bounding box in the
frame.

So for feedback purposes, the only parameters we need is the BBC (Bounding Box Code) and the label to give to this bounding box. The Video Generation in VPM ensures that recognized as well as unrecognized bounding boxes are shown with their BBCs. In the case of recognized bounding boxes, this helps because in case of an error, we can change the labelling of the face.

VPM only computes, for each scene, the celebrities that are present in it. The tight integration with FRM helps us to change the submodules of FRM easily, directly though VPM.
