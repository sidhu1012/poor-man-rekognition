from distutils.core import setup, Extension
from Cython.Build import cythonize
import numpy

package = Extension('rekognition.face_detection.bbox', ['rekognition/face_detection/bbox.pyx'], include_dirs=[numpy.get_include()], build_dir='build')

setup(name='Rekognition', ext_modules=cythonize([package]))
