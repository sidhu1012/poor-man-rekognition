DETECTOR_PARAMS = {
    "violaJones": [
        {
            "name": "xml_path (string)",
        },
        {
            "name": "scale_factor (float)",
        },
        {
            "name": "min_neighbors (integer)",
        },
        {
            "name": "batch_size (integer)",
        }
    ],
    "faceBoxes": [
        {
            "name": "thresh (float)",
        },
        {
            "name": "batch_size (integer)",
        }
    ]
};

ALIGNMENT_PARAMS = {
    "kazemiSullivan": [
        {
            "name": "model (string, compulsory)"
        },
        {
            "name": "batch_size (int)"
        }
    ],
    "None": [
        {
            "name": "batch_size (integer)"
        }
    ]
};

EMBEDDER_PARAMS = {
    "mobileFaceNet": [
        {
            "name": "model (string, compulsory)"
        },
        {
            "name": "batch_size (int)"
        }
    ]
};

var index = (function () {

	var onload = function (ctx, next) {
		console.log("Onload claled");
		//draw();
        Dropzone.options.videoDropzone = {
          url: '/upload_video',
          maxFiles: 1,
          accept: function(file, done) {
            console.log("uploaded");
            done();
          },
          init: function() {
            this.on("maxfilesexceeded", function(file){
                alert("No more files please!");
            });
            this.on('success', function() {
                $("#final-button").show()
            });
          }
        };
    }

	var ret = {};
    //ret.draw = draw;
	ret.onload = onload;
	return ret;

})();

function reset(){
    $('.dropzone')[0].dropzone.files.forEach(function(file) { 
      file.previewElement.remove(); 
    });
}

function train(){
    alertify.message('Starting Training...');
    $.ajax({
        type: "POST",
        url: "/train",
        success: function (response) {
            alertify.success('Finished Training Process!');
        },
    });
}

function addUnit(text, value){
    console.log(text, value);
}

function addCustomizedUnitOptions(text, value){
    $('.lined').show();
    $('#submission').removeClass('disabled');
    var selected = null;
    if(text==="FD"){
        selected = DETECTOR_PARAMS[value];
    }
    else if(text==="FA"){
        selected = ALIGNMENT_PARAMS[value];
    }
    else if(text==="FE"){
        selected = EMBEDDER_PARAMS[value];
    }
    var json = {
        "text": text,
        "value": value,
        "params": selected
    };
    var str = JSON.stringify(json, null, 2);
    $('.lined').val(str);
}

function addUnit(){
    alertify.message('Adding Unit...');
    $.ajax({
        type: "POST",
        url: "/add_unit",
        data: JSON.stringify($('.lined').val()),
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            if(response["success"]!=="success"){
                alertify.success('Please recheck the parameters!');
            }
            else{
                alertify.success('Unit added! Please retrain the system.');
            }
        },
    });
}


function addUnit(){
    alertify.message('Adding Unit...');
    $.ajax({
        type: "POST",
        url: "/add_unit",
        data: JSON.stringify($('.lined').val()),
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            if(response["success"]!=="success"){
                alertify.success('Please recheck the parameters!');
            }
            else{
                alertify.success('Unit added! Please retrain the system.');
            }
        },
    });
}

function detectFaces(){
    alertify.message('Starting face recognition, please wait...');
    $("#table-div").html("");
    $('#video-loc').css("display", "none");
    $('#correctionDiv').css("display", "none");
    $.ajax({
        method: "POST",
        url: '/detect_faces',
        success: function (response) {
            $("#video-dropzone").hide();
            $("#table-div").html(response["table"]);
            $("#videoButton").css("display", "block");
        }
    });
}

function generateVideo(){
    alertify.message('Generating video...');
    $("#table-div").html("");
    $.ajax({
        method: "POST",
        url: '/generate_video',
        success: function (response) {
            $('#videoButton').css("display", "none");
            $('#correctionDiv').css("display", "block");
            $('#video-loc source').attr('src', '/static/video/final.mp4');
            $('#video-loc').css("display", "block");
            $('#video-loc').show();
            $('#video-loc').load();
        }
    });
}

function giveFeedback(){
    alertify.message('Providing Feedback...');
    json = {
        "bbox_id": $("#bbox-tbox").val(),
        "label": $("#name-tbox").val()
    };
    $.ajax({
        method: "POST",
        url: '/give_feedback',
        data: JSON.stringify(json), 
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            $('#bbox-tbox').val("");
            $('#name-tbox').val("");
            alertify.message('Feedback taken...');
        }
    });
}
