from flask import Blueprint, request, session, jsonify, render_template, redirect, Response
import os
import cv2
from sqlalchemy.exc import IntegrityError
import app
from werkzeug.utils import secure_filename
import re
import pathlib
import pickle
from rekognition import VPM
from rekognition.face_alignment import AlignmentHashMap
from rekognition.face_identification import EmbedderHashMap
import json

mod_user = Blueprint('mod_user', __name__, url_prefix="")
ALLOWED_EXTENSIONS = set(['mp4', 'png', 'jpg', 'jpeg'])

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

@mod_user.route("/upload_photos", methods=["POST"])
def upload_photos():
    file = request.files['file']
    if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
    if file and allowed_file(file.filename):
        filename = secure_filename(file.filename)
        dr = 'server/dataset/' + '_'.join(filename.split('_')[0:-1]) + '/'
        pathlib.Path(dr).mkdir(parents=True, exist_ok=True) 
        file.save(dr + filename.split('_')[-1])
    return jsonify({"success":"success"})

@mod_user.route("/train", methods=["POST"])
def train():
    if app.userVPM is None:
        app.userVPM = VPM()
    app.userVPM.trainAllFaces('server/dataset/')
    return jsonify({"success":"success"})

@mod_user.route("/add_unit", methods=["POST"])
def add_unit():
    if app.userVPM is None:
        app.userVPM = VPM()
    data = request.data
    json_data = json.loads(json.loads(data))
    # clean json
    clean_kwargs = {}
    for param in json_data["params"]:
        if "value" in param and "name" in param:
            clean_kwargs[param["name"].split(' ')[0]] = param["value"]
    final_json = {
        "name": json_data["value"],
        "args": clean_kwargs    
    }
    # get object
    if json_data["text"] == "FD":
        app.userVPM.frm.initFaceDetector(final_json)
    elif json_data["text"] == "FA":
        app.userVPM.frm.initFaceAlignment(final_json)
    elif json_data["text"] == "FE":
        app.userVPM.frm.initFaceEmbedder(final_json)
    return jsonify({"success":"success"})

@mod_user.route("/upload_video", methods=["POST"])
def upload_video():
    file = request.files['file']
    print(file.filename)
    if file.filename == '':
        flash('No selected file')
        return redirect(request.url)
    if file and allowed_file(file.filename):
        filename = secure_filename(file.filename)
        dr = 'server/video/'
        pathlib.Path(dr).mkdir(parents=True, exist_ok=True) 
        file.save(dr + 'video.' + filename.split('.')[-1])
    return jsonify({"success":"success"})

@mod_user.route("/detect_faces", methods=["POST"])
def detect_faces():
    app.userVPM.getAllSceneDetails('server/video/video.mp4')
    # create table details
    table_str = '<table class="ui celled table"><thead><tr><th>#</th><th>Start-End Time</th>'+ \
        '<th>Celebrities</th></tr></thead><tbody>'
    for i in range(len(app.userVPM.all_scene_timings)):
        start_time = float(app.userVPM.all_scene_timings[i][0]) / app.userVPM.fps
        end_time = float(app.userVPM.all_scene_timings[i][1]) / app.userVPM.fps
        celebrities = app.userVPM.all_scene_details_verbose[i][0]
        table_str += '<tr><td>'+str(i+1)+'</td><td>'+str(round(start_time,3))+':'+str(round(end_time,3))+'</td><td>'
        for celeb in celebrities:
            table_str += ' '.join(celeb.split('_')) + '; '
        table_str += '</td></tr>'
    table_str += '</tbody></table>'
    return jsonify({"success":"success", "table":table_str})

@mod_user.route("/generate_video", methods=["POST"])
def generate_video():
    app.userVPM.generateVideo('app/static/video/final.mp4')
    return jsonify({"success":"success"})

@mod_user.route("/give_feedback", methods=["POST"])
def give_feedback():
    data = json.loads(request.data)
    app.userVPM.feedback(data)
    return jsonify({"success":"success"})
