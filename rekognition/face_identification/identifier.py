'''
Contains the class for performing Face Identification using
embedders (FaceIdentifier) and utility class FaceKNN
'''

import numpy as np
from sklearn.neighbors import NearestNeighbors

from .embedder_MobileFaceNet import MobileFaceNet 

class FaceKNN:

    '''
    Trains the KNN and results the label with nearest
    neighbour
    '''

    def __init__(self, embeddings, labels, thresh=0.95):
        '''
        Train the KNN model

        :param embeddings:
            :math:`N\times D` array of :math:`N` embeddings of dimension
            :math:`D`
        :type embeddings: ``list``
            
        :param labels:
            list of :math:`N` names of actors
        :type labels: ``list``
        '''
        self.KNN = NearestNeighbors(8, 0.4)
        self.labels = labels
        self.thresh = thresh
        self.KNN.fit(embeddings)

    def test(self, embeddings):
        '''
        Fit the KNN model to some embeddings

        :param embeddings:
            :math:`M\times D` array of :math:`M` face embeddings
        :type embeddings:: ``list``
        '''
        dist, ind = self.KNN.kneighbors(embeddings)
        result = []
        for i in range(len(embeddings)):
            if (self.labels[ind[i][0]] == self.labels[ind[i][1]] == self.labels[ind[i][2]] and
                    dist[i][0] < self.thresh):
                result.append(self.labels[ind[i][0]])
            else:
                result.append(None)
        return result

class FaceIdentifier:
    '''
    The Identifier wrapper class, performs embeddings generation
    and KNN training and testing
    '''

    def __init__(self, training_faces, training_labels, embedder=None):
        '''
        Train KNN model on faces and labels

        :param training_faces:
            list of :math:`N` faces
        :type training_faces: ``list``

        :param training_labels:
            list of :math:`N` face names corresponding to ``training_faces``
        :type training_labels: ``list``

        :param embedder:
            FaceEmbedder object (defaults to MobileFaceNet)
        :type embedder: ``list``
        '''
        if embedder is None:
            self.embedder = MobileFaceNet()
        else:
            self.embedder = embedder

        training_embeddings = self.embedder.generateEmbedding(training_faces)
        self.KNN = FaceKNN(training_embeddings, training_labels)


    def test(self, testing_faces):
        '''
        Test KNN on faces

        :param testing_faces:
            list of faces to be return the label for.
        :type testing_faces: ``list``

        :Returns:
            (``list``): list of labels for each face. ``None`` if no face isn't
            recognized.
        '''
        test_embeddings = self.embedder.generateEmbedding(testing_faces)
        result = self.KNN.test(test_embeddings)
        return result
