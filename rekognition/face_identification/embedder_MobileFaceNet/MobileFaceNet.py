'''Implementation of MobileFaceNet Face Embeddings using TensorFlow in MobileFaceNet class'''

import os

import cv2
import numpy as np
import tensorflow as tf

from ..base_embedder import FaceEmbedder

class MobileFaceNet(FaceEmbedder):
    r'''
    Calculates face embeddings using MobileFaceNet in TensorFlow.           
    '''

    def __init__(self, **kwargs):
        '''
        Creates the session object for running MobileFaceNet.
        '''
        super().__init__(**kwargs)

        model_path = os.path.join(os.path.dirname(__file__), 'facenet.pb')
        recognition_graph = tf.Graph()
        self.sess = tf.Session(graph=recognition_graph)
        with self.sess.as_default():
            with recognition_graph.as_default():
                with tf.gfile.GFile(model_path, 'rb') as f:
                    graph_def = tf.GraphDef()
                    graph_def.ParseFromString(f.read())
                    tf.import_graph_def(graph_def, name='')

        self.images_placeholder = recognition_graph.get_tensor_by_name("input:0")
        self.embeddings = recognition_graph.get_tensor_by_name("embeddings:0")
        self.phase_train_placeholder = recognition_graph.get_tensor_by_name("phase_train:0")
        self.embedding_size = self.embeddings.get_shape()[1]

    def prewhiten(self, faces):
        '''
        Performs mean averaging a list of faces
        '''
        transformed_faces = []
        for x in faces:
            mean = np.mean(x)
            std = np.std(x)
            std_adj = np.maximum(std, 1.0/np.sqrt(x.size))
            y = np.multiply(np.subtract(x, mean), 1/std_adj)
            y = cv2.resize(y, (160, 160), interpolation=cv2.INTER_AREA)
            transformed_faces.append(y)
        transformed_faces = np.array(transformed_faces)
        return transformed_faces

    def _embed(self, images):
        '''
        Embeds the faces.

        See ``base_embedder::_embed`` for more details.
        '''
        embeddings = self.sess.run(
           self.embeddings, feed_dict={self.images_placeholder: self.prewhiten(images), self.phase_train_placeholder: False}
        )
        return embeddings
