from .embedder_MobileFaceNet import MobileFaceNet

from .evaluator import FaceEmbedderEvaluator

from .identifier import FaceIdentifier

EmbedderHashMap = {
    "mobileFaceNet": MobileFaceNet
}
