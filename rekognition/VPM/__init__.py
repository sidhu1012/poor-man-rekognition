"Implementation of VPM: Video Processing Module"

import os

import cv2
from random import randrange

from ..FRM import FRM
from ..shot_segmentation import PSDSegmenter

class VPM:

    '''
    VPM (Video Processing Module) provides a higher layer than the FRM to integrate
    shot segmentation (or scene detection) with the face recognition processing. It
    also contains the methods to generate a video of the resulting recognized faces,
    and a coding for bounding boxes so that feedback can be considered.
    '''

    def __init__(self):
        '''
        Initializes FRM module
        '''
        self.frm = FRM()
        self.batch_size = 100

    def trainAllFaces(self, directory):
        '''
        Given a directory containing subdirectories of each celebrity,
        this method reads them into a format to directly supply FRM

        :param directory:
            path to directory
        :type directory: ``string``
        '''
        self.training_directory = directory
        image_labels = []
        image_list = []
        all_celebs = [file_name for file_name in os.listdir(directory) if file_name[0]!='.']
        for celeb in all_celebs:
            directory_name = os.path.join(directory, celeb)
            faces_file_names = [file_name for file_name in os.listdir(directory_name) if file_name[0]!='.']
            for file_name in faces_file_names:
                image_labels.append(celeb)
                image = cv2.cvtColor(cv2.imread(os.path.join(directory_name, file_name)), cv2.COLOR_BGR2RGB)
                image_list.append(cv2.resize(image, (150,200)))
        self.frm.train(image_list, image_labels)

    def getAllSceneDetails(self, video_name):
        '''
        Given the path to a video, this method does the crux of the processing:
        the video is segmented, and read scene by scene, while the faces are
        processed through FRM invokations

        :param video_name:
            path to the video file 
        :type video_name: ``string``
        '''
        self.video_name = video_name
        self.getSceneTimings(video_name) 

        self.all_scene_details_verbose = []
        self.all_scene_details_concise = []

        scene_no = 0
        frame_no = 0

        vidcap = cv2.VideoCapture(video_name)
        self.fps = vidcap.get(cv2.CAP_PROP_FPS)
        success, image = vidcap.read()

        image_buffer = [image]
        current_scene_details = [set(), []]
        
        while success:
            image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
            scene_no_change = False
            if frame_no >= self.all_scene_timings[scene_no][1]:
                scene_no_change = True

            if scene_no_change or len(image_buffer) >= self.batch_size:
                recognized_set, part_scene_details = self.runPartScene(image_buffer)
                current_scene_details[0] = current_scene_details[0].union(recognized_set)
                current_scene_details[1].extend(part_scene_details)

                if scene_no_change:
                    self.all_scene_details_verbose.append(current_scene_details)
                    self.all_scene_details_concise.append(self.conciseSceneDetails(current_scene_details[1]))
                    current_scene_details = [set(), []]
                image_buffer = []

            if scene_no_change:
                scene_no += 1
            frame_no += 1

            success, image = vidcap.read()
            image_buffer.append(image)

    def getSceneTimings(self, video_name):
        '''
        Calls shot segmenter to segment the video into scenes

        :param video_name:
            path to video file
        :type video_name: ``string``
        '''
        segmenter = PSDSegmenter(filename=video_name)
        self.all_scene_timings = segmenter.segment()

    def conciseSceneDetails(self, scene_details):
        '''
        A concise information of the scene, containing only
        the recognized faces and their bounding boxes information

        :param scene_details:
            verbose information of each scene
        :type scene_details: ``list``
        '''
        concise_details = []
        for image in scene_details:
            part_concise_details = []
            for i in range(0, len(image[0])):
                if image[0][i] is not None:
                    part_concise_details.append((image[0][i], image[1][i]))
            concise_details.append(part_concise_details)
        return concise_details

    def runPartScene(self, images):
        '''
        Run the recognition for images belonging to a single
        scene, by interfacing with FRM

        :param images:
            list of images belonging to same scene, and temporally
            consecutive
        :type images: ``list``
        '''
        part_scene_details = self.frm.test(images)
        recognized_set = set()
        for image in part_scene_details:
            for label in image[0]:
                recognized_set.add(label)
        if None in recognized_set:
            recognized_set.remove(None)
        return recognized_set, part_scene_details

    def generateVideo(self, destination):
        '''
        Generates a video of all detected bounding boxes with the
        recognized celebrities. Boxes are accompanied by a `code`,
        useful in performing the feedback learning.

        :param destination:
            path to destination video file
        :type destination: ``string``
        '''
        vidcap = cv2.VideoCapture(self.video_name)
        success, image = vidcap.read()

        fourcc = cv2.VideoWriter_fourcc(*'h264')
        eap = cv2.VideoWriter(destination, fourcc, 24, (int(vidcap.get(3)),int(vidcap.get(4))))

        scene_no = 0
        frame_no = 0
        mini_frame_no = 0

        while success:
            if frame_no > self.all_scene_timings[scene_no][1]:
                scene_no += 1
                mini_frame_no = 0

            for i in range(len(self.all_scene_details_verbose[scene_no][1][mini_frame_no][1])):
                code = str(scene_no) + ':' + str(mini_frame_no) + ':' + str(i)
                if self.all_scene_details_verbose[scene_no][1][mini_frame_no][0][i] is None:
                    (x,y,w,h) = self.all_scene_details_verbose[scene_no][1][mini_frame_no][1][i]
                    cv2.rectangle(image, (x,y), (x+w,y+h), (255,255,0), 2)
                    cv2.putText(image, code, (x,y), cv2.FONT_HERSHEY_PLAIN, 2, color=(255,255,0))
                else:
                    label = self.all_scene_details_verbose[scene_no][1][mini_frame_no][0][i]
                    (x,y,w,h) = self.all_scene_details_verbose[scene_no][1][mini_frame_no][1][i]
                    cv2.rectangle(image, (x,y), (x+w,y+h), (0,255,0), 2)
                    cv2.putText(image, code + ' ' + ' '.join(label.split('_')), (x,y), cv2.FONT_HERSHEY_PLAIN, 2, color=(0,255,0))

            eap.write(image)

            success, image = vidcap.read()
            frame_no += 1
            mini_frame_no += 1

    def feedback(self, data):
        '''
        Given a bounding box and its intended label, this function
        adds it to its dataset so that it can learn it.

        :param data:
            A dictionary containing the ``bbox_id`` and the label
            of this bounding box.
        :type data: ``dict``
        '''
        bbox_id = data["bbox_id"].split(':')
        scene = int(bbox_id[0])
        mini_frame = int(bbox_id[1])
        bbox = int(bbox_id[2])
        frame_no = self.all_scene_timings[scene][0] + mini_frame

        cap = cv2.VideoCapture(self.video_name)
        cap.set(cv2.CAP_PROP_POS_FRAMES, frame_no)
        res, frame = cap.read()

        if res:
            (x,y,w,h) = self.all_scene_details_verbose[scene][1][mini_frame][1][bbox]
            face = frame[int(y):int(y+h),int(x):int(x+w),:]
            iden = str(frame_no)+str(randrange(1000))+'.jpg'
            cv2.imwrite(os.path.join(self.training_directory, data["label"], iden), face)
