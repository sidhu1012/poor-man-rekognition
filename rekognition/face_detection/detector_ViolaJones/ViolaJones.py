'''Implementation of Viola-Jones Face Detection using OpenCV in ViolaJones class'''

import os

import cv2
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches

from ..base_detector import FaceDetector

class ViolaJones(FaceDetector):
    r'''
    Contains the Viola-Jones Face Detector implemented over the OpenCV ``CascadeClassifier``.

    .. note::
        Due to the use of ``CascadeClassifier`` which does not output the scores of the predictions,
        we have not performed the evaluation of ViolaJones.
    '''

    def __init__(self, xml_path=None, scale_factor=1.3, min_neighbors=5, **kwargs):
        '''
        :param xml_path:
            By default points to an available frontal face Haar cascade XML file.
        :type xml_path: ``string``

        :param scale_factor:
             Parameter specifying how much the image size is reduced at each image scale.
             By defauly 1.3.
        :type scale_factor: ``float``

        :param min_neighbors:
            Parameter specifying how many neighbors each candidate rectangle should have to retain it.
            By default 5.
        :type min_neighbors: ``int``

        :param \**kwargs:
            See ``base_detector`` ``__init__()`` parameters

        '''

        super().__init__(**kwargs)

        if xml_path is None:
            xml_path = os.path.join(os.path.dirname(__file__), 'haarcascade_frontalface_default.xml')

        self.scale_factor = scale_factor
        self.min_neighbors = min_neighbors

        self.classifier = cv2.CascadeClassifier()
        if not self.classifier.load(xml_path):
            raise FileNotFoundError('CascadeClassifier XML file not found')

    def _single_detect(self, image):
        '''
        :Returns:

            **faces** (``list``)
                Returns list of detected bounding boxes of the form :math:`[x,y,w,h]`.

        See ``base_detector::_single_detect`` for more details.
        '''
        if len(image.shape) == 3:
            image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        image = cv2.equalizeHist(image)
        faces = self.classifier.detectMultiScale(image, self.scale_factor, self.min_neighbors)

        return np.array(faces).tolist() 

    def _detect(self, images, offset=None):
        '''
        Since the OpenCV ``CascadeClassifier`` does not run parallelly on
        images, ``ViolaJones::_detect`` iteratively calls ``ViolaJones::_single_detect``.

        :Returns:

            **all_faces** (``list``)
                Returns list of detected bounding boxes of the faces in all images for 
                :math:`[o, [x,y,w,h]]` where :math:`o` is the image index for the detected 
                face (added to ``offset``, if supplied).

        See ``base_detector::_single_detect`` for more details.
        '''
        if offset is None:
            offset = 0

        all_faces = []
        for ind in range(len(images)):
            faces = self._single_detect(images[ind])
            for face in faces:
                all_faces.append([offset+ind, face])
        return all_faces, None
