# Poor Man's Rekognition

### GSoC 2019 Project for CCExtractor

#### Reports and Documentation

Checkout https://drcpmkeyi.gitlab.io/poor-man-rekognition for the three evaluation reports, as well as extensive documentation of Poor Man's Rekognition! The GUI Demo can be seen at https://www.youtube.com/watch?v=RpnpqfiQCOM :)

#### Installation Instructions:

Clone the repo with `GIT_LFS_SKIP_SMUDGE=1 git clone https://gitlab.com/drcpmkeyi/poor-man-rekognition.git`.

If you want to perform testing on the Face Detectors, run `git lfs fetch --include "tests/dataset/WIDER/**"`. Then `git lfs checkout .`. Now the file pointers/references should be replaced by the actual files. Similarly checkout the datasets only if required; the modules do not require it.

The reports show ways to easily interface with the module too. The basic steps will be:

1. `python3 -m virtualenv venv`
2. `source venv/bin/activate`
3. `pip install -r requirements.txt`
4. `from rekognition import FRM, VPM, face_detection` ...

To get the GUI running, supplement step 3. with `web\_requirements.txt`. Then run `python3 run.py`. The web-app can be accessed at `127.0.0.1:5000/ho`. Watch the GUI video linked in the report (III) for seeing how it works! 
